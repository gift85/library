<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180813010648 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX name ON book');
        $this->addSql('ALTER TABLE book ADD cover VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE book RENAME INDEX isbn TO UNIQ_CBE5A331CC1CF4E6');
        $this->addSql('DROP INDEX name ON author');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE UNIQUE INDEX name ON author (name, surname, patronymic)');
        $this->addSql('ALTER TABLE book DROP cover');
        $this->addSql('CREATE UNIQUE INDEX name ON book (name, year)');
        $this->addSql('ALTER TABLE book RENAME INDEX uniq_cbe5a331cc1cf4e6 TO isbn');
    }
}
