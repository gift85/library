<?php

namespace App\Controller;

use App\Entity\Book;
use App\Form\BookType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BookController
 */
class BookController extends AbstractController
{
    /**
     * @Route("/books", name="books")
     */
    public function index()
    {
        $books = $this->getDoctrine()->getRepository(Book::class)->findAll();

        return $this->render('book/index.html.twig', [
            'books' => $books
        ]);
    }

    /**
     * @Route("/book/{id<\d+>}", name="book")
     * @param Book $book
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Book $book)
    {
        return $this->render('book/show.html.twig', [
            'book' => $book, 'file' => new File($this->getParameter('covers_directory') . '/' . $book->getCover())
        ]);
    }

    /**
     * @Route("/book/create", name="book_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();

            return $this->redirectToRoute('book', ['id' => $book->getId()]);
        }

        return $this->render('book/create.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/book/{id<\d+>}/edit", name="book_edit")
     * @param Request $request
     * @param Book $book
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, Book $book)
    {
        if ($book->getCover()) {
            $book->setCover(
                new File($this->getParameter('covers_directory') . '/' . $book->getCover())
            );
        }

        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('book/edit.html.twig', [
                'form' => $form->createView()
            ]);
        }

        $coverFile = $form->get('cover')->getData();

        if ($coverFile instanceof UploadedFile) {
            $coverName = $book->getId() . $book->getName() . '_cover.' . $coverFile->guessExtension() ?? 'bin';
            $coverFile->move(
                $this->getParameter('covers_directory'),
                $coverName
            );
            $book->setCover($coverName);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($book);
        $entityManager->flush();

        return $this->redirectToRoute('book', ['id' => $book->getId()]);
    }

    /**
     * @Route("/book/{id<\d+>}/delete", name="book_delete")
     * @param Book $book
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Book $book)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($book);
        $entityManager->flush();
        return $this->redirectToRoute('books');
    }
}
