<?php

namespace App\Controller;

use App\Entity\Author;
use App\Form\AuthorType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AuthorController extends AbstractController
{
    /**
     * @Route("/authors", name="authors")
     */
    public function index()
    {
        $authors = $this->getDoctrine()->getRepository(Author::class)->findAll();

        return $this->render('author/index.html.twig', [
            'authors' => $authors
        ]);
    }

    /**
     * @Route("/author/create", name="author_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $author = new Author();
        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($author);
            $entityManager->flush();

            return $this->redirectToRoute('authors');
        }

        return $this->render('author/create.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/author/{id<\d+>}/edit", name="author_edit")
     * @param Request $request
     * @param Author $author
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, Author $author)
    {
        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($author);
            $entityManager->flush();

            return $this->redirectToRoute('authors');
        }
        return $this->render('author/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/author/{id<\d+>}/delete", name="author_delete")
     * @param Author $author
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Author $author)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($author);
        $entityManager->flush();
        return $this->redirectToRoute('authors');
    }
}
